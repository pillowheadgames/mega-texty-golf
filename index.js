const babelify = require("@babel/register").default;
babelify({ extensions: [".ts", ".js"] });
require("./src/index");
