import {
  User,
  Channel,
  TextChannel,
  GroupDMChannel,
  DMChannel
} from "discord.js";
import { None, PlayerState, PlayerStateList } from "./definitions/interfaces";
import { Course } from "~/course";
import { Logger } from "winston";

export class Game {
  private _playerState: PlayerStateList = {};
  private _minRoll = 1;
  private _maxRoll = 6;
  private _maxPower = 3;
  private _minPower = 0;
  private _channel: TextChannel | GroupDMChannel | DMChannel;
  private _running = true;
  private _courseIndex = 0;
  private _numCourses = 3;
  private _courses: Course[] = [];
  private _logger: Logger;

  constructor(
    logger: Logger,
    channel: TextChannel | GroupDMChannel | DMChannel
  ) {
    this._channel = channel;
    this._logger = logger.child({ name: "Game" });
    this._logger.debug("Initialising Game");
    for (let index = 0; index < this._numCourses; index++) {
      this._logger.debug(`Created course ${index + 1}`);
      this._courses.push(new Course(this._logger));
    }
    let interactionArray: string[] = [];
  }

  isPlayer(player: User) {
    this._logger.silly(`isPlayer called for player id ${player.id}`);
    if (!this._playerState[player.id]) {
      return false;
    }

    return true;
  }

  async tellPlayersAboutGame() {
    let interactionArray: string[] = [];
    interactionArray.push("A new game has started!");
    interactionArray.push(`This is a ${this._courses.length} course game`);
    interactionArray.push(`Here's what they look like:\n`);
    for (let c = 0; c < this._courses.length; c++) {
      let course = this._courses[c];
      interactionArray.push(course.getCourseAscii());
    }
    await this._channel.send(interactionArray.join("\n"));
  }

  async punch(player: User) {
    this._logger.silly(`punch action called for player id ${player.id}`);
    if (!this.isPlayer(player)) {
      this._logger.debug("Initialising player into game");
      this._playerState[player.id] = {
        object: player,
        score: 0,
        position: 0,
        moves: 0
      };
    }

    let power = this.getPower();
    let move = this.rollDice();
    if (power > 1 && move > 1) {
      await this._channel.send(
        `<@${
          player.id
        }> Punched 💨 the ball with ${power}x as much force as normal!`
      );
    } else {
      await this._channel.send(
        `<@${
          player.id
        }> punched the ball as calmly as Queen Elizabeth sipping a cup of english breakfast tea ☕`
      );
    }

    let playerState = this._playerState[player.id];
    this._logger.debug(
      `Punched with ${power}x power and a distance of ${move}`
    );
    let desiredSlot = playerState.position + power * move;

    if (desiredSlot > this.course.slots) {
      if (this.rollDice() % 2) {
        // Randomly let the ball rest into the hole
        // TODO: Somehow advance course for player and fix player state.
        playerState.position = 0;
        await this._channel.send(
          [
            `<@${
              player.id
            }>'s ball whisks towards the hole and despite all odds lands in the hole perfectly 🙌🎉🥂🎂`,
            "A new ball now waits on the tee:",
            this.course.getCourseAscii(playerState.position)
          ].join("\n")
        );
        this.setPlayerState(player.id, playerState);
        return;
      }

      this._logger.silly("Ball was punched into oblivion", {
        player: player.id
      });
      playerState.position = 0;
      await this._channel.send(
        [
          `<@${
            player.id
          }>'s ball soars into oblivion on the wings of a mighty aussie eagle 🌄🦅`,
          "A new ball now waits on the tee:",
          this.course.getCourseAscii(playerState.position)
        ].join("\n")
      );
      this.setPlayerState(player.id, playerState);
      return;
    }

    let interactionArray: string[] = [];
    this._logger.silly(`Ball is at slot ${playerState.position}`);
    let slotIndex = playerState.position + 1;
    for (; slotIndex < desiredSlot; slotIndex++) {
      let slot = this.course.getSlot(slotIndex);
      if (!slot) {
        this._logger.silly(`No obstacle for ${slotIndex}`);
        continue;
      }

      this._logger.debug(`Checking for interaction with ${slot.name}`);
      let interaction = slot.getInteraction();
      this._logger.debug("We should be interacting", interaction);
      interactionArray.push(<string>interaction.description);
      if (!interaction.destroyed && interaction.moves > 0) {
        this._logger.silly(
          `Ball is going to bounce back by ${interaction.moves}`
        );
        slotIndex -= interaction.moves;

        if (slotIndex < 0) {
          this._logger.silly(`Ball bounced back to the start`);
          slotIndex = 0;
        }
        this._logger.silly(`Ball is at slot ${slotIndex}`);
        break;
      } else if (interaction.destroyed) {
        this._logger.silly(`Ball destroyed`);
        slotIndex = -1;
        break;
      }
    }

    if (slotIndex < 0) {
      playerState.position = 0;
    } else {
      this._logger.silly(`Setting player position to ${slotIndex}`);
      playerState.position = slotIndex;
    }

    playerState.moves = playerState.moves + 1;

    if (playerState.position == this.course.slots) {
      interactionArray.push("The ball landed in the hole!");
      playerState.score = playerState.score + 1;
    } else if (playerState.position == 0) {
      interactionArray.push("Ball is resting on the tee");
    } else {
      let slot = this.course.getSlot(playerState.position);
      if (slot) {
        this._logger.debug(`Player is located in a position with ${slot.name}`);
        interactionArray.push(slot.rest);
      } else {
        interactionArray.push("The ball comes quietly to rest in a safe place");
      }
    }

    this.setPlayerState(player.id, playerState);

    this._logger.debug(
      `Sending message array to client ${playerState.position}`,
      interactionArray
    );
    interactionArray.push(this.course.getCourseAscii(playerState.position));
    await this._channel.send(interactionArray.join("\n"));
  }

  rollDice() {
    return Math.round(
      Math.random() * (+this._maxRoll - +this._minRoll) + +this._minRoll
    );
  }

  getPower() {
    return Math.ceil(
      Math.random() * (+this._maxPower - +this._minPower) + +this._minPower
    );
  }

  get course(): Course {
    return this._courses[this._courseIndex];
  }

  setPlayerState(id: string, value: PlayerState) {
    this._playerState[id] = value;
  }

  get finished(): boolean {
    for (let userKey in this._playerState) {
      let user = this._playerState[userKey];
      if (user.position !== this.course.slots) {
        // If the user is not at the end
        return false;
      }
    }

    return true;
  }
}
