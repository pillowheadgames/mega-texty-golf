import * as Discord from "discord.js";
import { Server } from "./server";
import { env } from "custom-env";
import * as winston from "winston";

const logger = winston.createLogger({
  level: process.env.LOG_LEVEL || "info",
  transports: [
    new winston.transports.File({ filename: "error.log", level: "error" }),
    new winston.transports.File({ filename: "all.log" })
  ]
});

// Initialize env variables from file .env[.<env-name>]
env(process.env.NODE_ENV);

const token = process.env.DISCORD_TOKEN || "unspecified";
const client = new Discord.Client();

// Use the console for logging when not in production
if (process.env.NODE_ENV != "production") {
  logger.add(
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.simple()
      )
    })
  );
}

const MegaTextyServer = new Server(client, logger, {
  name: "Mega Texty Golf"
});
MegaTextyServer.init(token, "/");
