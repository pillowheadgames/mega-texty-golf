import * as obstacles from "./obstacles";
import {
  CourseOptions,
  Obstacle,
  None,
  ObstacleConstructor
} from "./definitions/interfaces";
import { Logger } from "winston";

const defaultOptions: CourseOptions = {
  maxLength: 30,
  minLength: 15,
  obstacleDivisor: 5,
  bonusDivisor: 7
};

export class Course {
  private _length: number; // Length of the course
  private _slots: Array<Obstacle | None> = []; // Number of tiles in course
  private _uniqueObstacles: string[] = []; // Names of unique obstacles for a quick lookup
  private _obstacleChance: number = 0.3;
  private _logger: Logger;

  constructor(logger: Logger, options: CourseOptions = defaultOptions) {
    this._logger = logger.child({ name: "Course" });
    options = { ...defaultOptions, ...options };
    this._length = this._makeCourseLength(options.maxLength, options.minLength);
    this._populateSlots();
    this._logger.debug(`Course:\n${this.courseRepresentation().join("\n")}`);
  }

  public courseRepresentation() {
    let slotArray: string[] = [];
    for (let slotName in this._slots) {
      let slot = this._slots[slotName];
      if (slot) {
        slotArray.push(`[ ${slot.name} ]`);
      } else {
        slotArray.push("[ ]");
      }
    }

    return slotArray;
  }

  private _makeCourseLength(min: number, max: number): number {
    return Math.round(Math.random() * (+max - +min) + +min);
  }

  private _populateSlots() {
    this._slots = Array(this._length);

    for (let index = 0; index < this._length; index++) {
      if (index == this._length - 1 || index == 0) {
        // Skip the first + last hole... They have special meanings
        this._slots[index] = null;
        continue;
      }

      if (this._shouldBeObstacle()) {
        let maybeObstacle = this._getRandomObstacle();
        let obstacle: Obstacle | None = null;
        if (maybeObstacle != null) {
          obstacle = new maybeObstacle(this._logger);
        }
        this._slots[index] = obstacle;
        continue;
      }

      this._slots[index] = null;
    }
  }

  private _shouldBeObstacle() {
    const diceResult = Math.random();
    return diceResult <= this._obstacleChance;
  }

  private _getRandomObstacle(): ObstacleConstructor | None {
    const attempts = 5;
    let finalObstacle: ObstacleConstructor | None = null;

    for (let attempt = 1; attempt <= attempts; attempt++) {
      let obstacleList: ObstacleConstructor[] = [];
      for (let obstacleName in obstacles) {
        let obstacle = obstacles[obstacleName];
        obstacleList.push(obstacle);
      }
      let obstacle =
        obstacleList[Math.floor(Math.random() * obstacleList.length)];

      if (!this._uniqueObstacles.includes(obstacle.name)) {
        finalObstacle = obstacle;

        if (finalObstacle.unique == true) {
          this._uniqueObstacles.push(finalObstacle.name);
        }
        break;
      }
    }
    return finalObstacle;
  }

  getSlot(index: number): Obstacle | None {
    return this._slots[index];
  }

  getCourseAsciiArray(playerPosition: number | None = null): string[] {
    let courseAscii: string[] = [];
    for (let s = 0; s < this.slots; s++) {
      let slot = this._slots[s];
      if (playerPosition == s) {
        courseAscii.push("⚽");
      } else {
        // TODO: Visual representation of obstacle
        if (s + 1 == this.slots) {
          courseAscii.push("⛳");
        } else {
          if (slot) {
            courseAscii.push(slot.icon);
          }
          courseAscii.push("⬜");
        }
      }

      // courseAscii.push("|");
    }

    return courseAscii;
  }

  getCourseAscii(playerPosition: number | None = null): string {
    return this.getCourseAsciiArray(playerPosition).join("");
  }

  get slots() {
    return this._length;
  }
}
