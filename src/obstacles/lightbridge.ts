import { Obstacle, ObstacleResult } from "~/definitions/interfaces";
import { BaseObstacle } from "./baseobstacle";

const obstacleDescription = `
A magical bridge made purely out of light, that sometimes glitches because light's really hard to control man!`;

export class LightBridge extends BaseObstacle {
  public name = "Magical Bridge of Light";
  public description = obstacleDescription;
  public icon = "🌉";
  protected _chanceOfInteraction = 0.3;
  protected _restMessage =
    "His Greatness the Stupendeous protector of the magical bridge of light blesses your ball, as it makes its temporary home on the bed of photons.";

  public static unique = true;

  protected _possibilities = [
    {
      destroyed: true,
      description:
        "An evil magician fights with the protector of the magical bridge and overcomes him turning the bridge into a powerful weapon which eliminates anything that touches it. Your ball is obliterated."
    },
    {
      destroyed: true,
      description:
        "Just as your ball begins to pass over the bridge the Protector of the Magical bridge flushes the loo. Unfortunately the pump that fills the toilet is connected to the same powersource that powers the bridge and causes the bridge to momentarily glitch. Your ball passes through it and disappears into oblivion."
    },
    {
      destroyed: true,
      description:
        "Magical bridges of light aren't a thing right? Your unbelief causes the bridge to blink out of existence dropping your ball into infinite oblivion."
    },
    {
      destroyed: false,
      description: (moves: number) =>
        `A power surge occurs after the Magical Protector of the bridge laughs at the VR game "Watching Grass Grow In VR". The power surge turns the bridge into a ramp and the ball rolls back ${moves} spaces.`
    },
    {
      destroyed: false,
      description: (moves: number) =>
        `Your ball having built up a terrific shine from being punched around everywhere, reflects the light of the bridge sending it back ${moves}`
    }
  ];

  protected _safeResults = [
    "His Greatness the Stupendeous protector of the magical bridge waves as your ball passes over his bridge of glorious light.",
    "Your ball has such a trajectory, that it flys completely over the magical bridge of light",
    "Across the gap of oblivion a light bridge suddenly appears allowing your ball to pass over it safely.",
    "A guardian of time looks back to see that your ball is destined for greatness, and goes back in time to build a light bridge so that this very day, your ball will pass unharmed over the pit of oblivion."
  ];
}
