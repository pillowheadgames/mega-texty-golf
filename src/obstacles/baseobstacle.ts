import {
  Obstacle,
  ObstacleResult,
  ObstacleDescriptionFunc
} from "~/definitions/interfaces";
import { Logger } from "winston";

export class BaseObstacle implements Obstacle {
  public name: string = "BaseObstacle";
  public description: string = "Generic Description";
  public icon: string = "❌";
  protected _chanceOfInteraction = 0.1;
  protected _possibilities: {
    destroyed: boolean;
    description: string | ObstacleDescriptionFunc;
  }[] = [];
  protected _maxKnockback = 5;
  protected _minKnockback = 1;
  protected _safeResults;
  protected _canHaveMultiple = false; // Whether or not you can have multiple of this obstacle type on the course
  protected _logger: Logger;
  protected _restMessage: string = "The ball comes to a rest";
  public static unique: boolean = false;

  constructor(logger: Logger) {
    this._logger = logger.child({ name: this.constructor.name });
  }

  public getInteraction(): ObstacleResult {
    const diceResult = Math.random();
    let result = {
      destroyed: false,
      description: "",
      moves: 0
    };

    // If we happen to need to interact
    if (diceResult <= this._chanceOfInteraction) {
      // Get a random interaction
      let interaction = this._getRandomInteraction();
      if (
        !interaction.destroyed &&
        typeof interaction.description == "function"
      ) {
        // If we haven't destroyed the ball, set how far the ball will go back
        let knockback = this._getRandomKnockback();
        result.description = interaction.description(knockback);
        result.moves = knockback * -1;
      } else {
        // Ball was destroyed
        result.destroyed = true;
        result.description = <string>interaction.description;
      }
    } else {
      // The ball is safe!
      result.moves = -1; // Aka continue on, because destroyed is false
      result.description = this._getRandomSafeResult();
    }

    return result;
  }

  private _getRandomInteraction() {
    return this._possibilities[
      Math.floor(Math.random() * this._possibilities.length)
    ];
  }
  private _getRandomSafeResult(): string {
    return this._safeResults[
      Math.floor(Math.random() * this._safeResults.length)
    ];
  }

  private _getRandomKnockback(): number {
    return Math.round(
      Math.random() * (+this._maxKnockback - +this._minKnockback) +
        +this._minKnockback
    );
  }

  get multipleAllowed() {
    return this._canHaveMultiple;
  }

  get rest() {
    return this._restMessage;
  }
}
