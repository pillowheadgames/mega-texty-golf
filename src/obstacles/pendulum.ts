import { Obstacle, ObstacleResult } from "~/definitions/interfaces";
import { BaseObstacle } from "./baseobstacle";

const obstacleDescription = `
Across the course swings a razor sharp pendulum that may cut your ball in half or knock it into oblivion.`;

export class Pendulum extends BaseObstacle {
  public name = "Pendulum of Doom";
  public description = obstacleDescription;
  public icon = "⚔️";
  protected _chanceOfInteraction = 0.1;
  protected _restMessage =
    "By sheer luck, the ball rests on the far side of the Pendulum of Doom unscathed";

  public static unique = true;

  protected _possibilities = [
    {
      destroyed: true,
      description:
        "The Pendulum of Doom impales the ball with it's sharp pointed edge."
    },
    {
      destroyed: true,
      description:
        "The Pendulum of Doom slices cleanly through the ball and it splits apart, never to be punched again."
    },
    {
      destroyed: true,
      description:
        "Despite it's incredible sharpness, your ball manages to avoid being sliced by the Pendulum of doom. Unfortunately, it still manages to knock your ball off the side and into oblivion."
    },
    {
      destroyed: false,
      description: (moves: number) =>
        `By pure luck, your ball manages to bounce off the Pendulum of doom and moves back ${moves} spaces.`
    }
  ];

  protected _safeResults = [
    "Regardless of the likelihood your ball might be impaled, your ball manages to narrowly avoid the sharp edge of the Pendulum of Doom",
    "Swiftly your ball passes underneath the Pendulum of Doom",
    "Without regard for a healthy fear of God, your ball zooms past the Pendulum of Doom",
    "Somehow the power of your punch causes the fabric of time and space to rip and the ball phases through the Pendulum of Doom as if it were a common Gnome"
  ];
}
