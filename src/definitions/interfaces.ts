import { Guild, Channel, User } from "discord.js";
import { Game } from "~/game";
import { Logger } from "winston";
import { BaseObstacle } from "~/obstacles/baseobstacle";

export interface ServerOptions {
  name?: string;
  disabledFunctions?: string[];
}

export type None = null | undefined;

export interface Obstacle {
  name: string;
  description: string;
  icon: string;
  getInteraction(): ObstacleResult;
  rest: string;
}
export interface ObstacleConstructor {
  new (logger: Logger): Obstacle;
  unique: boolean;
}

export interface ObstacleResult {
  moves: number;
  destroyed: boolean;
  description: string | ObstacleDescriptionFunc;
}

export interface ObstacleDescriptionFunc {
  (number): string;
}

export interface CourseOptions {
  maxLength: number;
  minLength: number;
  obstacleDivisor: number;
  bonusDivisor: number;
}

export interface GameList {
  guild: Guild;
  channel: Channel;
  game: Game;
}

export interface PlayerStateList {
  [key: string]: PlayerState;
}

export interface PlayerState {
  object: User;
  position: number;
  score: number;
  moves: number;
}
