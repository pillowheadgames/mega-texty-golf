export class Player {
  name: string;
  tag: string;
  position: number = 0;

  constructor(name: string, tag: string) {
    this.name = name;
    this.tag = tag;
  }
}
