import { Command } from "~/commands/command";
import { Message, Client } from "discord.js";
import { Game } from "~/game";
import { None } from "~/definitions/interfaces";
import { loggers } from "winston";

export class Punch extends Command {
  protected readonly _commandRegex = [/punch/i];
  protected _currentGames: { string?: Game | None } = {};

  public async invoke(message: Message) {
    if (!this._currentGames[message.guild.id]) {
      this._logger.debug("No Current Game. Starting One");
      this._currentGames[message.guild.id] = new Game(
        this._logger,
        message.channel
      );
      this._currentGames[message.guild.id].tellPlayersAboutGame();
    }
    await this._currentGames[message.guild.id].punch(message.author);

    this._logger.debug("Command called");
  }
}
