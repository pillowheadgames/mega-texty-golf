import { Message, Client } from "discord.js";
import { Logger } from "winston";

export abstract class Command {
  protected _commandRegex: RegExp[] = [];
  protected _logger: Logger;
  protected _client: Client;

  constructor(logger: Logger, client: Client) {
    this._client = client;
    this._logger = logger.child({ command: this.constructor.name });
  }

  public setCommand(command: RegExp | RegExp[] | string) {
    let regex: RegExp[] = [];

    if (typeof command === "string") {
      regex = [new RegExp(`/${command}/`)];
    } else if (!Array.isArray(command)) {
      regex = [command];
    }

    this._commandRegex = <RegExp[]>command;
  }

  public isValid(message: string): boolean {
    if (this._commandRegex.length < 1) {
      return false;
    }
    let result: boolean = false;
    this._commandRegex.forEach(check => {
      if (check.test(message.toLowerCase()) === true) {
        result = true;
        return;
      }
    });

    return result;
  }

  get name() {
    return this.constructor.name;
  }

  public abstract invoke(message: Message);
}

export default Command;
