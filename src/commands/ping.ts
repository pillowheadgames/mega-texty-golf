import { Command } from "~/commands/command";
import { Message, Client } from "discord.js";

export class Ping extends Command {
  protected readonly _commandRegex = [/ping/i];

  public async invoke(message: Message) {
    this._logger.debug("Command called");
    let response = (await message.channel.send("Ping?")) as any;
    this._logger.debug("Sent ping message");
    try {
      let result = await response.edit(`Pong!
        Latency is ${response.createdTimestamp - message.createdTimestamp}ms.
        API Latency is ${Math.round(this._client.ping)}ms
      `);
      this._logger.debug("Edited ping message");
    } catch (error) {
      this._logger.error("Failed to edit ping message", error);
    }
  }
}
