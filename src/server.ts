import * as Discord from "discord.js";
import { Logger } from "winston";
import { ServerOptions, None } from "~/definitions/interfaces";
import { Command } from "./commands/command";
import * as commands from "./commands";

export class Server {
  private _client: Discord.Client;
  private _prefix: string = "/mega";
  private _serverName: string = "Mega Texty Golf";
  private _logger: Logger;
  private _commands: Command[] = [];
  private _disabledCommands: string[] = [];

  constructor(
    client: Discord.Client,
    logger: Logger,
    options: ServerOptions | None = null
  ) {
    this._client = client;
    this._logger = logger;

    if (options != null) {
      this._serverName = options.name || this._serverName;
    }
  }

  public init(token: string, prefix: string = "/") {
    this._client.on("disconnect", this.handleDisconnect);
    // this._client.on("error", this.handleError);
    this._client.once("ready", this.onReady);
    this._client.on("message", this.handleMessage);
    // this._client.on("debug", this.handleError);
    this._client.login(token).catch(this.handleError);

    this._initCommands();
  }

  private _initCommands() {
    this._logger.debug("Logging Commands", commands);
    for (let commandName in commands) {
      if (this._disabledCommands.includes(commandName.toLowerCase())) {
        this._logger.debug(`Ignoring disabled command ${commandName}`);
        continue;
      }

      this._logger.debug(`Initialising command ${commandName}`);
      this._commands.push(
        new commands[commandName](this._logger, this._client)
      );
    }
  }

  public onReady = () => {
    this._logger.info("Server Started");
  };

  public handleError = (error: Error) => {
    this._logger.error(error.message);
  };

  public handleDisconnect = (event: CloseEvent) => {
    this._logger.debug(event.reason);
  };

  handleMessage = async (message: Discord.Message) => {
    if (!message.cleanContent.startsWith(this._prefix)) {
      return;
    }

    const subMessage = message.cleanContent.slice(this._prefix.length + 1);

    this._commands.forEach(async command => {
      if (command.isValid(subMessage)) {
        this._logger.debug(`Invoking command ${command.name}`);
        await command.invoke(message);
      }
    });

    this._logger.debug(`Server received message ${message.content}`);

    return new Promise<string>((resolve, reject) => {});
  };
}
